import { writable } from 'svelte/store'

export const name = writable('')
export const names = writable([] as string[])
